package stackoverflow

import org.scalatest.{BeforeAndAfterAll, FunSuite}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.rdd.RDD
import java.net.URL
import java.nio.channels.Channels
import java.io.File
import java.io.FileOutputStream

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class StackOverflowSuite extends FunSuite with BeforeAndAfterAll {

  val postingsList = List(
    Posting(1, 1, None, None, 0, Some("Java")),
    Posting(2, 2, None, Some(1), 15, None),
    Posting(2, 3, None, Some(1), 5, None),
    Posting(2, 4, None, Some(1), 13, None),
    Posting(1, 5, None, None, 0, Some("PHP")),
    Posting(1, 6, None, None, 0, Some("Haskell")),
    Posting(2, 7, None, Some(6), 3, None)
  )

  lazy val testObject = new StackOverflow {
    override val langs =
      List(
        "JavaScript", "Java", "PHP", "Python", "C#", "C++", "Ruby", "CSS",
        "Objective-C", "Perl", "Scala", "Haskell", "MATLAB", "Clojure", "Groovy")
    override def langSpread = 50000
    override def kmeansKernels = 45
    override def kmeansEta: Double = 20.0D
    override def kmeansMaxIterations = 120
  }

  test("testObject can be instantiated") {
    val instantiatable = try {
      testObject
      true
    } catch {
      case _: Throwable => false
    }
    assert(instantiatable, "Can't instantiate a StackOverflow object")
  }

  test("'groupedPostings' should work correctly") {
    val postings = StackOverflow.sc.parallelize(postingsList)

    val grouped = testObject.groupedPostings(postings).collectAsMap()
    val firstQuestion = grouped(1)
    assert(firstQuestion.size == 3)
    assert(firstQuestion.count(_._1.id == 1) == 3)
    assert(firstQuestion.count(_._2.id == 2) == 1)
    assert(firstQuestion.count(_._2.id == 3) == 1)
    assert(firstQuestion.count(_._2.id == 4) == 1)

    val secondQuestion = grouped.get(5)
    assert(secondQuestion.isEmpty)

    val thirdQuestion = grouped(6)
    assert(thirdQuestion.size == 1)
    assert(thirdQuestion.last._1.id == 6)
    assert(thirdQuestion.last._2.id == 7)
  }

  test("'scoredPostings' should work correctly") {
    val postings = StackOverflow.sc.parallelize(postingsList)
    val grouped = testObject.groupedPostings(postings)
    val scored = testObject.scoredPostings(grouped).collect()

    assert(scored.length == 2)

    scored.foreach {
      pair =>
        pair._1.id match {
          case 1 => assert(pair._2 == 15)
          case 6 => assert(pair._2 == 3)
          case _ => assert(false, pair.toString())
        }
    }
  }

  test("'vectorPostings' should work correctly") {
    val postings = StackOverflow.sc.parallelize(postingsList)
    val grouped = testObject.groupedPostings(postings)
    val scored = testObject.scoredPostings(grouped)
    val vectors = testObject.vectorPostings(scored).collect()

    assert(vectors.length == 2)

    val forJava = vectors.filter(_._1 == 1 * StackOverflow.langSpread).last
    val forHaskell = vectors.filter(_._1 == 11 * StackOverflow.langSpread).last

    assert(forJava._2 == 15)
    assert(forHaskell._2 == 3)
  }

  test("'kmeans' should work with three samples") {

    def genThree(lang: Int): Traversable[(Int, Int)] = {
      val x = lang * testObject.langSpread
      List((x, Random.nextInt(100)), (x, Random.nextInt(100)), (x, Random.nextInt(100)))
    }

    val vectors = StackOverflow.sc.parallelize(testObject.langs.indices.flatMap(genThree))
    var means = testObject.sampleVectors(vectors)

    var newMeans = testObject.kmeans(means, vectors)

    means = means.sorted
    newMeans = newMeans.sorted

    assert(means.length == newMeans.length)
  }
}
